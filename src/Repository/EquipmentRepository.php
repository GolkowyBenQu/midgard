<?php

namespace App\Repository;

use App\Entity\Equipment;
use App\Entity\EquipmentItemSlot;
use App\Entity\Player;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class EquipmentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Equipment::class);
    }

    /**
     * @param Player $player
     * @return Equipment|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getEquipment(Player $player)
    {
        $qb = $this->createQueryBuilder('e');

        return $qb
            ->leftJoin('e.owner', 'o')
            ->leftJoin('e.items', 'is')
            ->leftJoin('is.item', 'it')
            ->leftJoin('it.type', 't')
            ->leftJoin('it.quality', 'q')
            ->leftJoin('it.itemAttributes', 'ia')
            ->leftJoin('ia.attribute', 'at')
            ->andWhere($qb->expr()->eq('o.id', $player->getId()))

//            ->andWhere('e.owner', $player)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getSlots(Player $player)
    {
        $qb = $this->createQueryBuilder('e');

        return $qb
            ->select('s')
            ->from(EquipmentItemSlot::class, 's')
            ->getQuery()
            ->getResult();
    }
}
