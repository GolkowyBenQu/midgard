<?php

namespace App\Repository;

use App\Entity\ItemSlot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ItemSlot|null find($id, $lockMode = null, $lockVersion = null)
 * @method ItemSlot|null findOneBy(array $criteria, array $orderBy = null)
 * @method ItemSlot[]    findAll()
 * @method ItemSlot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemSlotRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ItemSlot::class);
    }
}
