<?php

namespace App\Repository;

use App\Entity\ItemType;
use App\Entity\Player;
use App\Entity\PlayerItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlayerItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlayerItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlayerItem[]    findAll()
 * @method PlayerItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlayerItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlayerItem::class);
    }

    /**
     * @param Player $player
     * @param string $eqType
     * @return PlayerItem[]
     */
    public function getPlayerItemsForPlayerByEqType(Player $player, string $eqType): array
    {
        $qb = $this->createQueryBuilder('pi');

        return $qb
            ->leftJoin('pi.player', 'pl')
            ->leftJoin('pi.equipmentType', 'et')
            ->leftJoin('pi.item', 'i')
            ->where('pl.id = :playerId')
            ->andWhere('et.name = :eqTypeName')
            ->setParameter('playerId', $player->getId())
            ->setParameter('eqTypeName', $eqType)
            ->orderBy('i.id')
            ->getQuery()
            ->getResult();
    }
}
