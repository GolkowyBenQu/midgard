<?php

namespace App\Repository;

use App\Entity\PlayerItemSlot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlayerItemSlot|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlayerItemSlot|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlayerItemSlot[]    findAll()
 * @method PlayerItemSlot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlayerItemSlotRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlayerItemSlot::class);
    }
}
