<?php

namespace App\Repository;

use App\Entity\Location;
use App\Entity\Place;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class PlaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Place::class);
    }

    public function findByLocation(Location $location)
    {
        $qb = $this->createQueryBuilder('p');

        return $qb
            ->leftJoin('p.locations', 'l')
            ->andWhere($qb->expr()->eq('l.id', $location->getId()))
            ->getQuery()
            ->getResult();
    }
}
