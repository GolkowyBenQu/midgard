<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class DoctrineDatabaseRefreshCommand extends Command
{
    protected static $defaultName = 'doctrine:database:refresh';

    private $commandsToRun = [
        'doctrine:database:drop' => [
            '--force' => true,
        ],
        'doctrine:database:create' => [],
        'doctrine:migrations:migrate' => [],
        'doctrine:fixtures:load' => [
            '--append' => true,
        ],
    ];

    protected function configure(): void
    {
        $this
            ->setDescription('Refresh configured database');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        foreach ($this->commandsToRun as $commandName => $commandArguments) {
            $command = $this->getApplication()->find($commandName);
            $commandInput = new ArrayInput($commandArguments);
            $commandInput->setInteractive(false);
            $returnCode = $command->run($commandInput, $output);

            if ($returnCode === 0) {
                $io->success($commandName.' done with success');
            } else {
                $io->warning($commandName.' done with return code '.$returnCode);
            }
        }

        return 0;
    }
}
