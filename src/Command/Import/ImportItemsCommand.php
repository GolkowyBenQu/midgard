<?php

namespace App\Command\Import;

use App\Service\ImportService;
use App\ValueObject\Import\ImportResult;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImportItemsCommand extends Command
{
    protected static $defaultName = 'app:import:items';

    /** @var ImportService */
    private $importService;

    public function __construct(
        ImportService $importService
    ) {
        parent::__construct();
        $this->importService = $importService;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Imports items from CSV')
            ->addArgument('path', InputArgument::REQUIRED, 'CSV file path');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        ini_set('memory_limit', '1024M');

        $io = new SymfonyStyle($input, $output);

        $path = $input->getArgument('path');

        $result = $this->importService->importItems($path);

        foreach ($result[ImportResult::ERROR_MESSAGES] as $error) {
            $io->error($error);
        }

        if (isset($result[ImportResult::ADDED_ROWS_COUNT])) {
            $io->success('Proper rows added: '.$result[ImportResult::ADDED_ROWS_COUNT]);
        }

        if (isset($result[ImportResult::UPDATED_ROWS_COUNT])) {
            $io->success('Proper rows updated: '.$result[ImportResult::UPDATED_ROWS_COUNT]);
        }

        if (isset($result[ImportResult::INVALID_ROWS_COUNT])) {
            $io->success('Invalid rows: '.$result[ImportResult::INVALID_ROWS_COUNT]);
        }
    }
}
