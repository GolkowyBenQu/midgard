<?php

namespace App\Service;

use App\Entity\AnnouncementCategory;
use App\Entity\City;
use App\Entity\Item;
use App\Entity\ProductAttribute;
use App\Entity\ProductCategory;
use App\Entity\ProductTemplate;
use App\Entity\ProductValue;
use App\Repository\AnnouncementCategoryRepository;
use App\Repository\CityRepository;
use App\Repository\ItemRepository;
use App\Repository\ProductAttributeRepository;
use App\Repository\ProductCategoryRepository;
use App\Repository\ProductTemplateRepository;
use App\Repository\ProductValueRepository;
use App\Repository\ProvinceRepository;
use App\ValueObject\Import\ImportField;
use App\ValueObject\Import\ImportResult;
use App\ValueObject\ProductAttributeType;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Exception as LeagueCsvException;
use League\Csv\Reader;

class ImportService
{
    private const PACKAGE_LIMIT = 100;

    private ItemRepository $itemRepository;

    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(
        ItemRepository $itemRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->itemRepository = $itemRepository;
        $this->entityManager = $entityManager;
    }

    public function importItems(string $path): array
    {
        $addedRows = 0;
        $invalidRows = 0;
        $updatedRows = 0;
        $errors = [];

        $csv = $this->loadCsvFromPath($path);

        $csvRows = $csv->getRecords([
            ImportField::IMPORT_ID_FIELD,
            ImportField::NAME_FIELD,
            ImportField::PRICE_FIELD,
            ImportField::WEARABLE_FIELD,
            ImportField::USABLE_FIELD,
            ImportField::TYPE_FIELD,
            ImportField::QUALITY_FIELD,
            ImportField::SLOT_FIELD,
            ImportField::STACK_SIZE_FIELD,
        ]);

        foreach ($csvRows as $index => $csvRow) {
            $csvRow = $this->filterEmptyValues($csvRow);

            $item = $this->itemRepository->findOneByImportId($csvRow[ImportField::IMPORT_ID_FIELD]);
            if (!$item) {
                $item = new Item();
                $addedRows++;
            } else {
                $updatedRows++;
            }

            $item->setName($csvRow[ImportField::NAME_FIELD]);
            $item->setPrice($csvRow[ImportField::PRICE_FIELD]);
            $item->setWearable($csvRow[ImportField::WEARABLE_FIELD]);
            $item->setUsable($csvRow[ImportField::USABLE_FIELD]);
            $item->setType($csvRow[ImportField::TYPE_FIELD]);
            $item->setQuality($csvRow[ImportField::QUALITY_FIELD]);
            $item->setSlot($csvRow[ImportField::SLOT_FIELD] ?? '');
            $item->setStackSize($csvRow[ImportField::STACK_SIZE_FIELD] ?? 1);
            $item->setImportId($csvRow[ImportField::IMPORT_ID_FIELD]);

            $this->entityManager->persist($item);
            $this->entityManager->flush();
        }

        $this->entityManager->flush();

        return [
            ImportResult::ADDED_ROWS_COUNT => $addedRows,
            ImportResult::INVALID_ROWS_COUNT => $invalidRows,
            ImportResult::UPDATED_ROWS_COUNT => $updatedRows,
            ImportResult::ERROR_MESSAGES => $errors,
        ];
    }

    private function loadCsvFromPath(string $path): Reader
    {
        try {
            $csv = Reader::createFromPath($path);
            $csv->setHeaderOffset(0);
            $csv->setDelimiter(';');
        } catch (LeagueCsvException $e) {
            throw new \RuntimeException('Cant read import file');
        }

        return $csv;
    }

    private function filterEmptyValues(array $csvRow): array
    {
        return array_filter(
            $csvRow,
            static function ($value) {
                return $value !== '' && $value !== null;
            }
        );
    }
}
