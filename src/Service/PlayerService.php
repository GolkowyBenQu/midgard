<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Attribute;
use App\Entity\Location;
use App\Entity\Player;
use App\Entity\PlayerAttribute;
use App\Entity\PlayerItemSlot;
use App\Entity\Race;
use App\Entity\RaceAttribute;
use App\Entity\User;
use App\Repository\AttributeRepository;
use App\Repository\ItemSlotRepository;
use App\Repository\LocationRepository;
use Doctrine\ORM\EntityManagerInterface;

final class PlayerService
{
    private AttributeRepository $attributeRepository;
    private ItemService $itemService;
    private ItemSlotRepository $itemSlotRepository;
    private LocationRepository $locationRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(
        AttributeRepository $attributeRepository,
        ItemSlotRepository $itemSlotRepository,
        LocationRepository $locationRepository,
        EntityManagerInterface $entityManager,
        ItemService $itemService
    ) {
        $this->attributeRepository = $attributeRepository;
        $this->itemService = $itemService;
        $this->itemSlotRepository = $itemSlotRepository;
        $this->locationRepository = $locationRepository;
        $this->entityManager = $entityManager;
    }

    public function updatePlayerStats(Player $player, bool $overrideBase = false): void
    {
        $strengthPa = $player->getPlayerAttribute(Attribute::STRENGTH_ATTRIBUTE);
        $dexterityPa = $player->getPlayerAttribute(Attribute::DEXTERITY_ATTRIBUTE);
        $vitalityPa = $player->getPlayerAttribute(Attribute::VITALITY_ATTRIBUTE);
        $energyPa = $player->getPlayerAttribute(Attribute::ENERGY_ATTRIBUTE);

        $strengthValue = $strengthPa->getCurrentValue() * $strengthPa->getAttribute()->getPointValue();
        $dexterityValue = $dexterityPa->getCurrentValue() * $dexterityPa->getAttribute()->getPointValue();
        $vitalityValue = $vitalityPa->getCurrentValue() * $vitalityPa->getAttribute()->getPointValue();
        $energyValue = $energyPa->getCurrentValue() * $energyPa->getAttribute()->getPointValue();

        $damagePa = $player->getPlayerAttribute(Attribute::DAMAGE_ATTRIBUTE);
        $defensePa = $player->getPlayerAttribute(Attribute::DEFENSE_ATTRIBUTE);
        $lifePa = $player->getPlayerAttribute(Attribute::LIFE_ATTRIBUTE);
        $manaPa = $player->getPlayerAttribute(Attribute::MANA_ATTRIBUTE);

        $damagePa->setCurrentValue(
            (int)($damagePa->getBaseValue() + $strengthValue * Attribute::STRENGTH_TO_DAMAGE_MULTIPLIER)
        );
        $defensePa->setCurrentValue(
            (int)($defensePa->getCurrentValue() +
            $strengthValue * Attribute::STRENGTH_TO_DEFENSE_MULTIPLIER +
            $dexterityValue * Attribute::DEXTERITY_TO_DEFENSE_MULTIPLIER)
        );
        $lifePa->setCurrentValue(
            (int)($lifePa->getBaseValue() + $vitalityValue * Attribute::VITALITY_TO_LIFE_MULTIPLIER)
        );
        $manaPa->setCurrentValue(
            (int)($manaPa->getBaseValue() + $energyValue * Attribute::ENERGY_TO_MANA_MULTIPLIER)
        );

        if ($overrideBase) {
            $damagePa->setBaseValue($damagePa->getCurrentValue());
            $defensePa->setBaseValue($defensePa->getCurrentValue());
            $lifePa->setBaseValue($lifePa->getCurrentValue());
            $manaPa->setBaseValue($manaPa->getCurrentValue());
        }
    }

    public function createFullPlayer(Player $player, User $user): void
    {
        $player->setUser($user);
        $player->setExperience(0);

        $this->generateSlots($player);
        $this->generateAttributes($player);
        $this->updatePlayerStats($player, true);

        $race = $player->getRace();
        $this->itemService->givePlayerItem($player, $race->getDefaultItem());
        $player->setCurrentLocation($this->locationRepository->findOneByName(Location::DEFAULT_LOCATION));
    }

    private function generateAttributes(Player $player)
    {
        $attributes = $this->attributeRepository->findAll();
        foreach ($attributes as $attribute) {
            $playerAttribute = new PlayerAttribute();
            $playerAttribute->setAttribute($attribute);
            $playerAttribute->setBaseValue(1);
            $playerAttribute->setCurrentValue(1);
            $playerAttribute->setPlayer($player);

            $this->entityManager->persist($playerAttribute);

            $player->addPlayerAttribute($playerAttribute);
        }

        /** @var Race $race */
        $race = $player->getRace();

        /** @var RaceAttribute $raceAttribute */
        foreach ($race->getRaceAttributes() as $raceAttribute) {
            $playerAttriute = $player->getPlayerAttribute($raceAttribute->getAttribute()->getName());
            $playerAttribute->setCurrentValue($raceAttribute->getValue());
            $playerAttribute->setBaseValue($raceAttribute->getValue());
            $this->entityManager->persist($playerAttribute);
        }
    }

    private function generateSlots(Player $player)
    {
        $slots = $this->itemSlotRepository->findAll();
        foreach ($slots as $slot) {
            $playerItemSlot = new PlayerItemSlot();
            $playerItemSlot->setSlot($slot);
            $playerItemSlot->setPlayer($player);
            $player->addPlayerItemSlot($playerItemSlot);

            $this->entityManager->persist($playerItemSlot);
        }
    }
}
