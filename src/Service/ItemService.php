<?php

namespace App\Service;

use App\Entity\EquipmentType;
use App\Entity\Item;
use App\Entity\ItemType;
use App\Entity\Player;
use App\Entity\PlayerItem;
use App\Repository\EquipmentTypeRepository;
use App\Strategy\ItemInterface;
use App\Strategy\ItemManager;
use Doctrine\ORM\EntityManagerInterface;

class ItemService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ItemManager
     */
    private $itemManager;
    /**
     * @var EquipmentTypeRepository
     */
    private $equipmentTypeRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        ItemManager $itemManager,
        EquipmentTypeRepository $equipmentTypeRepository
    ) {
        $this->entityManager = $entityManager;
        $this->itemManager = $itemManager;
        $this->equipmentTypeRepository = $equipmentTypeRepository;
    }

    public function playerHasItem(Player $player, Item $item): bool
    {
        foreach ($player->getPlayerItems() as $ownedItem) {
            if ($ownedItem->getItem()->getId() === $item->getId()) {
                return true;
            }
        }

        return false;
    }

//    public function incrementPlayerStack(Player $player, Item $item, int $count = 1): void
//    {
//        foreach ($player->getPlayerItems() as $ownedItem) {
//            if ($ownedItem->getItem()->getId() === $item->getId()) {
//                $ownedItem->setCount($ownedItem->getCount() + $count);
//            }
//        }
//    }

    public function recalculatePlayerStacks(Player $player, Item $item, int $addCount = 0): void
    {
        $itemsCount = $addCount;

        /** @var PlayerItem[] $stacks */
        $stacks = [];
        foreach ($player->getPlayerItems() as $playerItem) {
            if ($playerItem->getItem()->getId() === $item->getId()) {
                $stacks[] = $playerItem;
                $itemsCount += $playerItem->getCount();
            }
        }

        foreach ($stacks as $playerItemStack) {
            if ($itemsCount <= 0) {
                $this->entityManager->remove($playerItemStack);
            }

            $newCount = $playerItemStack->getItem()->getStackSize();

            if ($itemsCount < $newCount) {
                $newCount = $itemsCount;
            }

            $playerItemStack->setCount($newCount);
            $this->entityManager->persist($playerItemStack);

            $itemsCount -= $newCount;
        }

        if ($itemsCount > 0) {
            $this->createNewPlayerItem($player, $item, $itemsCount);
        }
    }

//    public function decrementPlayerStack(Player $player, Item $item, int $count = 1): void
//    {
//        foreach ($player->getPlayerItems() as $ownedItem) {
//            if ($ownedItem->getItem()->getId() === $item->getId()) {
//                $ownedItem->setCount($ownedItem->getCount() - $count);
//            }
//
//            if ($ownedItem->getCount() <= 0) {
//                $this->removePlayerItem($player, $item);
//            }
//        }
//    }

    public function createNewPlayerItem(Player $player, Item $item, int $addCount = 1): void
    {
        /** @var EquipmentType $eqType */
        $eqType = $this->equipmentTypeRepository->findOneByName(EquipmentType::BAG_TYPE);

        while ($addCount > 0) {
            $newCount = $item->getStackSize() ?? 1;

            if ($addCount < $newCount) {
                $newCount = $addCount;
            }

            $playerItem = new PlayerItem();
            $playerItem->setItem($item);
            $playerItem->setCount($newCount);
            $playerItem->setPlayer($player);
            $playerItem->setEquipmentType($eqType);

            $player->addPlayerItem($playerItem);
            $this->tryToPut($player, $playerItem);

            $this->entityManager->persist($playerItem);

            $addCount -= $newCount;
        }

    }

    public function givePlayerItem(Player $player, Item $item, int $count = 1): void
    {
        if (!$item->getStackSize()) {
            $this->createNewPlayerItem($player, $item, $count);
            return;
        }

        $hasItem = $this->playerHasItem($player, $item);

        if ($hasItem) {
            $this->recalculatePlayerStacks($player, $item, $count);
        } else {
            $this->createNewPlayerItem($player, $item, $count);
        }
    }

//    public function removePlayerItem(Player $player, Item $item): void
//    {
//        foreach ($player->getPlayerItems() as $playerItem) {
//            if ($playerItem->getItem()->getId() === $item->getId()) {
//                $player->removePlayerItem($playerItem);
//            }
//        }
//
//        $this->entityManager->persist($player);
//        $this->entityManager->flush();
//    }

    public function useItemOnPlayer(Player $player, PlayerItem $playerItem): void
    {
        $used = false;

        /** @var ItemInterface $useStrategy */
        $useStrategy = $this->itemManager->getStrategy($playerItem->getItem());

        $useStrategy->updatePlayer($player, $playerItem);
        $this->entityManager->persist($player);

        $this->entityManager->flush();

//        if ($used) {
//            $this->removePlayerItem($player, $item);
//        }
    }

    public function buyItem(Player $player, Item $item)
    {
        // mirian - złoty talar
        // canath - srebrny grosz = 1/4 mirian
    }

    private function tryToPut(Player $player, PlayerItem $playerItem)
    {
        $allowedTypes = [
            ItemType::WEAPON_TYPE,
            ItemType::ARMOR_TYPE,
        ];
        $itemType = $playerItem->getItem()->getType();

        if (!in_array($itemType, $allowedTypes)) {
            return;
        }

        $itemSlot = $playerItem->getItem()->getSlot();
        $playerItemSlot = $player->getPlayerItemSlot($itemSlot->getName());

        if (!$playerItemSlot) {
            return;
        }

        if ($playerItemSlot->getPlayerItem()) {
            return;
        }

        /** @var ItemInterface $useStrategy */
        $useStrategy = $this->itemManager->getStrategy($playerItem->getItem());
        $useStrategy->updatePlayer($player, $playerItem);
    }
}
