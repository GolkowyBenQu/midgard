<?php

namespace App\Service;

use App\Entity\Ingredient;
use App\Entity\Player;
use App\Entity\Receipe;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;

class CraftingService
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    private $receipeRepository;

    /**
     * @var ItemService
     */
    private $itemService;

    public function __construct(
        EntityManagerInterface $entityManager,
        ItemService $itemService
    )
    {
        $this->receipeRepository = $entityManager->getRepository(Receipe::class);
        $this->itemService = $itemService;
    }

    /**
     * @param Player $player
     * @return Receipe[]
     */
    public function getReceipesForPlayer(Player $player): array
    {
        $allReceipes = $this->receipeRepository->findAll();

        $ownedItems = [];
        foreach ($player->getPlayerItems() as $playerItem) {
            $ownedItems[$playerItem->getItem()->getId()] = $playerItem->getCount();
        }

        $receipes = [];
        /** @var Receipe $receipe */
        foreach ($allReceipes as $receipe) {
            $receipeItems = [];
            $availableCount = 0;
            foreach ($receipe->getIngredients() as $ingredient) {
                $requiredCount = $ingredient->getCount();
                $itemId = $ingredient->getItem()->getId();

                $availableItem = false;
                if (isset($ownedItems[$itemId]) && $ownedItems[$itemId] >= $requiredCount) {
                    $availableItem = true;
                    $availableCount += 1;
                }

                $receipeItems[$itemId] = [
                    'item' => $ingredient->getItem(),
                    'count' => $ingredient->getCount(),
                    'available' => $availableItem,
                ];
            }

            $availableReceipe = false;
            if ($availableCount === count($receipe->getIngredients())) {
                $availableReceipe = true;
            }

            $receipes[] = [
                'receipe' => $receipe,
                'available' => $availableReceipe,
                'receipeItems' => $receipeItems,
            ];
        }

        return $receipes;
    }

    /**
     * @param Player $player
     * @param Collection $ingredients
     */
    public function removePlayerIngredients(
        Player $player,
        Collection $ingredients
    ): void
    {
        /** @var Ingredient $ingredient */
        foreach ($ingredients as $ingredient) {
            $this->itemService->recalculatePlayerStacks($player, $ingredient->getItem(), $ingredient->getCount() * -1);
        }
    }

    public function craftRecipe(Player $player, Receipe $receipe)
    {
        $this->removePlayerIngredients($player, $receipe->getIngredients());
        $this->itemService->givePlayerItem($player, $receipe->getItem());
    }
}
