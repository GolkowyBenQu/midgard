<?php

namespace App\Service;

use App\Entity\Player;
use App\Entity\Monster;

class FightService
{
    public $actions = [];

    /**
     * @param Player $player
     * @param Monster $monster
     */
    public function fightWithMonster(Player $player, Monster $monster): void
    {
        $monsterLife = $monster->getLife();
        $playerLife = $player->getPlayerAttribute('life')->getCurrentValue();

        while ($playerLife > 0 && $monsterLife > 0) {
            $playerDamage = $player->getPlayerAttribute('damage')->getCurrentValue();
            $monsterDamage = $monster->getDamage();

            $monsterLife = ($monsterLife - $playerDamage > 0) ? $monsterLife - $playerDamage : 0;
            $this->actions[] = [
                'attacker' => $player->getUser(),
                'attackerLife' => $player->getPlayerAttribute('life')->getCurrentValue(),
                'attackerDamage' => $playerDamage,
                'defender' => $monster,
                'defenderLife' => $monsterLife,
                'defenderDamage' => $monsterDamage,
            ];

            if ($monsterLife <= 0) {
                break;
            }

            $playerLife -= $monsterDamage;
            $player->getPlayerAttribute('life')->setCurrentValue(
                $player->getPlayerAttribute('life')->getCurrentValue() - $monsterDamage
            );
            $this->actions[] = [
                'attacker' => $monster,
                'attackerLife' => $monsterLife,
                'attackerDamage' => $monsterDamage,
                'defender' => $player->getUser(),
                'defenderLife' => $player->getPlayerAttribute('life')->getCurrentValue(),
                'defenderDamage' => $playerDamage,
            ];
        }
    }
}
