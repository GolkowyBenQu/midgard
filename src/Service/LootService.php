<?php

namespace App\Service;

use App\Entity\Item;
use App\Entity\Monster;

class LootService
{
    /**
     * @param Monster $monster
     * @param int $count
     * @return Item[]
     */
    public function randomLoot(Monster $monster, int $count = 0): array
    {
        $lootItems = [];
        $itemsCount = 0;

        foreach ($monster->getLoots() as $loot) {
            $itemChance = $loot->getChance();
            $randChance = rand(0, 100);

            if ($randChance >= $itemChance) {
                $lootItems[] = [
                    'item' => $loot->getItem(),
                    'count' => 1,
                ];
                $itemsCount += 1;
            }

            if ($count && $itemsCount >= $count) {
                break;
            }
        }

        return $lootItems;
    }
}
