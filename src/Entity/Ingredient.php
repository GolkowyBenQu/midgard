<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Ingredient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Receipe", inversedBy="ingredients")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $receipe;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Item", inversedBy="ingredients")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $item;

    /**
     * @ORM\Column(type="integer")
     */
    private $count;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReceipe(): ?Receipe
    {
        return $this->receipe;
    }

    public function setReceipe(?Receipe $receipe): self
    {
        $this->receipe = $receipe;

        return $this;
    }

    public function getItem(): ?Item
    {
        return $this->item;
    }

    public function setItem(?Item $item): self
    {
        $this->item = $item;

        return $this;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }
}
