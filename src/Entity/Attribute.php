<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AttributeRepository")
 */
class Attribute
{
    public const DAMAGE_ATTRIBUTE = 'damage';
    public const DEFENSE_ATTRIBUTE = 'defense';
    public const SPEED_ATTRIBUTE = 'speed';
    public const LIFE_ATTRIBUTE = 'life';
    public const MANA_ATTRIBUTE = 'mana';
    public const STRENGTH_ATTRIBUTE = 'strength';
    public const DEXTERITY_ATTRIBUTE = 'dexterity';
    public const VITALITY_ATTRIBUTE = 'vitality';
    public const ENERGY_ATTRIBUTE = 'energy';

    public const STRENGTH_TO_DAMAGE_MULTIPLIER = 1.2;
    public const STRENGTH_TO_DEFENSE_MULTIPLIER = 0.6;
    public const DEXTERITY_TO_DEFENSE_MULTIPLIER = 0.6;
    public const VITALITY_TO_LIFE_MULTIPLIER = 20;
    public const ENERGY_TO_MANA_MULTIPLIER = 1.2;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $pointValue;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isBasic;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPointValue(): ?float
    {
        return $this->pointValue;
    }

    public function setPointValue(float $pointValue): self
    {
        $this->pointValue = $pointValue;

        return $this;
    }

    public function getIsBasic(): ?bool
    {
        return $this->isBasic;
    }

    public function setIsBasic(?bool $isBasic): self
    {
        $this->isBasic = $isBasic;

        return $this;
    }
}
