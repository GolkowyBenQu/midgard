<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EquipmentTypeRepository")
 */
class EquipmentType
{
    public const BAG_TYPE = 'bag';
    public const SAFE_TYPE = 'safe';
    public const AUCTION_TYPE = 'auction';
    public const SLOT_TYPE = 'slot';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlayerItem", mappedBy="equipmentType", orphanRemoval=true)
     */
    private $playerItems;

    public function __construct()
    {
        $this->equipment = new ArrayCollection();
        $this->playerItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|PlayerItem[]
     */
    public function getPlayerItems(): Collection
    {
        return $this->playerItems;
    }

    public function addPlayerItem(PlayerItem $playerItem): self
    {
        if (!$this->playerItems->contains($playerItem)) {
            $this->playerItems[] = $playerItem;
            $playerItem->setEquipmentType($this);
        }

        return $this;
    }

    public function removePlayerItem(PlayerItem $playerItem): self
    {
        if ($this->playerItems->contains($playerItem)) {
            $this->playerItems->removeElement($playerItem);
            // set the owning side to null (unless already changed)
            if ($playerItem->getEquipmentType() === $this) {
                $playerItem->setEquipmentType(null);
            }
        }

        return $this;
    }
}
