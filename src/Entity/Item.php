<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ItemRepository")
 */
class Item
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /** @ORM\Column(type="string", length=255) */
    private ?string $name;

    /** @ORM\Column(type="integer") */
    private ?int $price;

    /** @ORM\OneToMany(targetEntity="Loot", mappedBy="item", orphanRemoval=true) */
    private $loots;

    /** @ORM\Column(type="boolean") */
    private ?bool $wearable;

    /** @ORM\Column(type="boolean") */
    private ?bool $usable;

    /** @ORM\OneToMany(targetEntity="App\Entity\Receipe", mappedBy="item", orphanRemoval=true) */
    private $receipes;

    /** @ORM\OneToMany(targetEntity="App\Entity\Ingredient", mappedBy="item", orphanRemoval=true) */
    private $ingredients;

    /** @ORM\OneToMany(targetEntity="App\Entity\ItemAttribute", mappedBy="item", orphanRemoval=true) */
    private $itemAttributes;

    /** @ORM\Column(type="string", length=255) */
    private $type;

    /** @ORM\Column(type="string", length=255) */
    private $quality;

    /** @ORM\Column(type="string", length=255) */
    private $slot;

    /** @ORM\OneToMany(targetEntity="App\Entity\PlayerItem", mappedBy="item", orphanRemoval=true) */
    private $playerItems;

    /** @ORM\OneToMany(targetEntity="App\Entity\Race", mappedBy="defaultItem") */
    private $races;

    /** @ORM\Column(type="integer") */
    private $stackSize = 0;

    /** @ORM\Column(type="integer") */
    private $importId;

    public function __construct()
    {
        $this->loots = new ArrayCollection();
        $this->receipes = new ArrayCollection();
        $this->ingredients = new ArrayCollection();
        $this->itemAttributes = new ArrayCollection();
        $this->playerItems = new ArrayCollection();
        $this->races = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return Collection|Loot[]
     */
    public function getLoots(): Collection
    {
        return $this->loots;
    }

    public function addLoot(Loot $loot): self
    {
        if (!$this->loots->contains($loot)) {
            $this->loots[] = $loot;
            $loot->setItem($this);
        }

        return $this;
    }

    public function removeLoot(Loot $loot): self
    {
        if ($this->loots->contains($loot)) {
            $this->loots->removeElement($loot);
            // set the owning side to null (unless already changed)
            if ($loot->getItem() === $this) {
                $loot->setItem(null);
            }
        }

        return $this;
    }

    public function getWearable(): ?bool
    {
        return $this->wearable;
    }

    public function setWearable(bool $wearable): self
    {
        $this->wearable = $wearable;

        return $this;
    }

    public function getUsable(): ?bool
    {
        return $this->usable;
    }

    public function setUsable(?bool $usable): self
    {
        $this->usable = $usable;

        return $this;
    }

    /**
     * @return Collection|Receipe[]
     */
    public function getReceipes(): Collection
    {
        return $this->receipes;
    }

    public function addReceipe(Receipe $receipe): self
    {
        if (!$this->receipes->contains($receipe)) {
            $this->receipes[] = $receipe;
            $receipe->setItem($this);
        }

        return $this;
    }

    public function removeReceipe(Receipe $receipe): self
    {
        if ($this->receipes->contains($receipe)) {
            $this->receipes->removeElement($receipe);
            // set the owning side to null (unless already changed)
            if ($receipe->getItem() === $this) {
                $receipe->setItem(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Ingredient[]
     */
    public function getIngredients(): Collection
    {
        return $this->ingredients;
    }

    public function addIngredient(Ingredient $ingredient): self
    {
        if (!$this->ingredients->contains($ingredient)) {
            $this->ingredients[] = $ingredient;
            $ingredient->setItem($this);
        }

        return $this;
    }

    public function removeIngredient(Ingredient $ingredient): self
    {
        if ($this->ingredients->contains($ingredient)) {
            $this->ingredients->removeElement($ingredient);
            // set the owning side to null (unless already changed)
            if ($ingredient->getItem() === $this) {
                $ingredient->setItem(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ItemAttribute[]
     */
    public function getItemAttributes(): Collection
    {
        return $this->itemAttributes;
    }

    public function addItemAttribute(ItemAttribute $itemAttribute): self
    {
        if (!$this->itemAttributes->contains($itemAttribute)) {
            $this->itemAttributes[] = $itemAttribute;
            $itemAttribute->setItem($this);
        }

        return $this;
    }

    public function removeItemAttribute(ItemAttribute $itemAttribute): self
    {
        if ($this->itemAttributes->contains($itemAttribute)) {
            $this->itemAttributes->removeElement($itemAttribute);
            // set the owning side to null (unless already changed)
            if ($itemAttribute->getItem() === $this) {
                $itemAttribute->setItem(null);
            }
        }

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getQuality(): ?string
    {
        return $this->quality;
    }

    public function setQuality(?string $quality): self
    {
        $this->quality = $quality;

        return $this;
    }

    public function getSlot(): ?string
    {
        return $this->slot;
    }

    public function setSlot(?string $slot): self
    {
        $this->slot = $slot;

        return $this;
    }

    /**
     * @return Collection|PlayerItem[]
     */
    public function getPlayerItems(): Collection
    {
        return $this->playerItems;
    }

    public function addPlayerItem(PlayerItem $playerItem): self
    {
        if (!$this->playerItems->contains($playerItem)) {
            $this->playerItems[] = $playerItem;
            $playerItem->setItem($this);
        }

        return $this;
    }

    public function removePlayerItem(PlayerItem $playerItem): self
    {
        if ($this->playerItems->contains($playerItem)) {
            $this->playerItems->removeElement($playerItem);
            // set the owning side to null (unless already changed)
            if ($playerItem->getItem() === $this) {
                $playerItem->setItem(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Race[]
     */
    public function getRaces(): Collection
    {
        return $this->races;
    }

    public function addRace(Race $race): self
    {
        if (!$this->races->contains($race)) {
            $this->races[] = $race;
            $race->setDefaultItem($this);
        }

        return $this;
    }

    public function removeRace(Race $race): self
    {
        if ($this->races->contains($race)) {
            $this->races->removeElement($race);
            // set the owning side to null (unless already changed)
            if ($race->getDefaultItem() === $this) {
                $race->setDefaultItem(null);
            }
        }

        return $this;
    }

    public function getStackSize(): ?int
    {
        return $this->stackSize;
    }

    public function setStackSize(int $stackSize): self
    {
        $this->stackSize = $stackSize;

        return $this;
    }

    public function getImportId()
    {
        return $this->importId;
    }

    public function setImportId($importId): void
    {
        $this->importId = $importId;
    }
}
