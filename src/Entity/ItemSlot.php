<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ItemSlotRepository")
 */
class ItemSlot
{
    public const PRIMARY_WEAPON_SLOT = 'primaryWeapon';
    public const SECONDARY_WEAPON_SLOT = 'secondaryWeapon';
    public const HEAD_SLOT = 'head';
    public const CHEST_SLOT = 'chest';
    public const HANDS_SLOT = 'hands';
    public const LEGS_SLOT = 'legs';
    public const FEET_SLOT = 'feet';
    public const WAIST_SLOT = 'waist';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlayerItemSlot", mappedBy="slot", orphanRemoval=true)
     */
    private $playerItemSlots;

    public function __construct()
    {
        $this->playerItemSlots = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|EquipmentItemSlot[]
     */
    public function getPlayerItemSlots(): Collection
    {
        return $this->playerItemSlots;
    }

    public function addPlayerItemSlot(PlayerItemSlot $playerItemSlot): self
    {
        if (!$this->playerItemSlots->contains($playerItemSlot)) {
            $this->playerItemSlots[] = $playerItemSlot;
            $playerItemSlot->setSlot($this);
        }

        return $this;
    }

    public function removePlayerItemSlot(PlayerItemSlot $playerItemSlot): self
    {
        if ($this->playerItemSlots->contains($playerItemSlot)) {
            $this->playerItemSlots->removeElement($playerItemSlot);
            // set the owning side to null (unless already changed)
            if ($playerItemSlot->getSlot() === $this) {
                $playerItemSlot->setSlot(null);
            }
        }

        return $this;
    }
}
