<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Loot
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Monster", inversedBy="loots")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $monster;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Item", inversedBy="loots")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $item;

    /**
     * @ORM\Column(type="smallint")
     */
    private $chance;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMonster(): ?Monster
    {
        return $this->monster;
    }

    public function setMonster(?Monster $monster): self
    {
        $this->monster = $monster;

        return $this;
    }

    public function getItem(): ?Item
    {
        return $this->item;
    }

    public function setItem(?Item $item): self
    {
        $this->item = $item;

        return $this;
    }

    public function getChance(): ?int
    {
        return $this->chance;
    }

    public function setChance(int $chance): self
    {
        $this->chance = $chance;

        return $this;
    }
}
