<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class PlayerItemSlot
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlayerItem", inversedBy="playerItemSlots")
     * @ORM\JoinColumn(nullable=true)
     */
    private $playerItem;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ItemSlot", inversedBy="playerItemSlots")
     * @ORM\JoinColumn(nullable=false)
     */
    private $slot;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Player", inversedBy="playerItemSlots")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $player;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlayerItem(): ?PlayerItem
    {
        return $this->playerItem;
    }

    public function setPlayerItem(?PlayerItem $playerItem): self
    {
        $this->playerItem = $playerItem;

        return $this;
    }

    public function getSlot(): ?ItemSlot
    {
        return $this->slot;
    }

    public function setSlot(?ItemSlot $slot): self
    {
        $this->slot = $slot;

        return $this;
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    public function setPlayer(?Player $player): self
    {
        $this->player = $player;

        return $this;
    }
}
