<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Race
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Player", mappedBy="currentLocation")
     */
    private $players;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RaceAttribute", mappedBy="race", orphanRemoval=true)
     */
    private $raceAttributes;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Item", inversedBy="races")
     * @ORM\JoinColumn(nullable=false, onDelete="SET NULL")
     */
    private $defaultItem;

    public function __construct()
    {
        $this->players = new ArrayCollection();
        $this->raceAttributes = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Player[]
     */
    public function getPlayers(): Collection
    {
        return $this->players;
    }

    public function addPlayer(Player $player): self
    {
        if (!$this->players->contains($player)) {
            $this->players[] = $player;
            $player->setRace($this);
        }

        return $this;
    }

    public function removePlayer(Player $player): self
    {
        if ($this->players->contains($player)) {
            $this->players->removeElement($player);
            // set the owning side to null (unless already changed)
            if ($player->getRace() === $this) {
                $player->setRace(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RaceAttribute[]
     */
    public function getRaceAttributes(): Collection
    {
        return $this->raceAttributes;
    }

    public function addRaceAttribute(RaceAttribute $raceAttribute): self
    {
        if (!$this->raceAttributes->contains($raceAttribute)) {
            $this->raceAttributes[] = $raceAttribute;
            $raceAttribute->setRace($this);
        }

        return $this;
    }

    public function removeRaceAttribute(RaceAttribute $raceAttribute): self
    {
        if ($this->raceAttributes->contains($raceAttribute)) {
            $this->raceAttributes->removeElement($raceAttribute);
            // set the owning side to null (unless already changed)
            if ($raceAttribute->getRace() === $this) {
                $raceAttribute->setRace(null);
            }
        }

        return $this;
    }

    public function getDefaultItem(): ?Item
    {
        return $this->defaultItem;
    }

    public function setDefaultItem(?Item $defaultItem): self
    {
        $this->defaultItem = $defaultItem;

        return $this;
    }
}
