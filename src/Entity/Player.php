<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/** @ORM\Entity */
class Player
{
    public const DEFAULT_LIFE = 100;
    public const DEFAULT_DAMAGE = 5;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="player", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $experience;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Location", inversedBy="players")
     */
    private $currentLocation;

    /**
     * @Assert\NotNull
     * @ORM\ManyToOne(targetEntity="App\Entity\Race", inversedBy="players")
     */
    private $race;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlayerAttribute", mappedBy="player", orphanRemoval=true)
     */
    private $playerAttributes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlayerItemSlot", mappedBy="player", cascade={"persist", "remove"})
     */
    private $playerItemSlots;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlayerItem", mappedBy="player", orphanRemoval=true)
     */
    private $playerItems;

    public function __construct()
    {
        $this->playerAttributes = new ArrayCollection();
        $this->playerItemSlots = new ArrayCollection();
        $this->playerItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getExperience(): ?int
    {
        return $this->experience;
    }

    public function setExperience(?int $experience): self
    {
        $this->experience = $experience;

        return $this;
    }

    public function getCurrentLocation(): ?Location
    {
        return $this->currentLocation;
    }

    public function setCurrentLocation(?Location $currentLocation): self
    {
        $this->currentLocation = $currentLocation;

        return $this;
    }

    /**
     * @return Collection|PlayerAttribute[]
     */
    public function getPlayerAttributes(): Collection
    {
        return $this->playerAttributes;
    }

    public function addPlayerAttribute(PlayerAttribute $playerAttribute): self
    {
        if (!$this->playerAttributes->contains($playerAttribute)) {
            $this->playerAttributes[] = $playerAttribute;
            $playerAttribute->setPlayer($this);
        }

        return $this;
    }

    public function removePlayerAttribute(PlayerAttribute $playerAttribute): self
    {
        if ($this->playerAttributes->contains($playerAttribute)) {
            $this->playerAttributes->removeElement($playerAttribute);
            // set the owning side to null (unless already changed)
            if ($playerAttribute->getPlayer() === $this) {
                $playerAttribute->setPlayer(null);
            }
        }

        return $this;
    }

    /**
     * @param string $name
     * @return PlayerAttribute|null
     */
    public function getPlayerAttribute(string $name):? PlayerAttribute
    {
        foreach ($this->getPlayerAttributes() as $playerAttribute) {
            if ($playerAttribute->getAttribute()->getName() === $name) {
                return $playerAttribute;
            }
        }

        return null;
    }

    public function getPlayerItemSlot(string $name):? PlayerItemSlot
    {
        foreach ($this->getPlayerItemSlots() as $playerItemSlot) {
            if ($playerItemSlot->getSlot()->getName() === $name) {
                return $playerItemSlot;
            }
        }

        return null;
    }

    public function getRace():? Race
    {
        return $this->race;
    }

    public function setRace(Race $race): self
    {
        $this->race = $race;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|PlayerItem[]
     */
    public function getPlayerItems(): Collection
    {
        return $this->playerItems;
    }

    public function addPlayerItem(PlayerItem $playerItem): self
    {
        if (!$this->playerItems->contains($playerItem)) {
            $this->playerItems[] = $playerItem;
            $playerItem->setPlayer($this);
        }

        return $this;
    }

    public function removePlayerItem(PlayerItem $playerItem): self
    {
        if ($this->playerItems->contains($playerItem)) {
            $this->playerItems->removeElement($playerItem);
            // set the owning side to null (unless already changed)
            if ($playerItem->getPlayer() === $this) {
                $playerItem->setPlayer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PlayerItemSlot[]
     */
    public function getPlayerItemSlots()
    {
        return $this->playerItemSlots;
    }

    public function addPlayerItemSlot(PlayerItemSlot $playerItemSlot): self
    {
        if (!$this->playerItemSlots->contains($playerItemSlot)) {
            $this->playerItemSlots[] = $playerItemSlot;
            $playerItemSlot->setPlayer($this);
        }

        return $this;
    }

    public function removePlayerItemSlot(PlayerItemSlot $playerItemSlot): self
    {
        if ($this->playerItemSlots->contains($playerItemSlot)) {
            $this->playerItemSlots->removeElement($playerItemSlot);
            // set the owning side to null (unless already changed)
            if ($playerItemSlot->getPlayer() === $this) {
                $playerItemSlot->setPlayer(null);
            }
        }

        return $this;
    }
}
