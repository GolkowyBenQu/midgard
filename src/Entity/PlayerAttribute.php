<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlayerAttributeRepository")
 */
class PlayerAttribute
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Player", inversedBy="playerAttributes")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $player;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Attribute", inversedBy="playerAttributes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $attribute;

    /**
     * @ORM\Column(type="integer")
     */
    private $currentValue;

    /**
     * @ORM\Column(type="integer")
     */
    private $baseValue;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    public function setPlayer(?Player $player): self
    {
        $this->player = $player;

        return $this;
    }

    public function getAttribute(): ?Attribute
    {
        return $this->attribute;
    }

    public function setAttribute(?Attribute $attribute): self
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function getCurrentValue(): ?int
    {
        return $this->currentValue;
    }

    public function setCurrentValue(int $currentValue): self
    {
        $this->currentValue = $currentValue;

        return $this;
    }

    public function getBaseValue(): ?int
    {
        return $this->baseValue;
    }

    public function setBaseValue(int $baseValue): self
    {
        $this->baseValue = $baseValue;

        return $this;
    }
}
