<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Place;
use App\Entity\Player;
use App\Repository\PlaceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class PlaceController extends AbstractController
{
    /**
     * @Route("/place", name="place")
     */
    public function index(): Response
    {
        /** @var Player $player */
        $player = $this->getUser()->getPlayer();

        $em = $this->getDoctrine()->getManager();

        /** @var PlaceRepository $placeRepository */
        $placeRepository = $em->getRepository(Place::class);
        $places = $placeRepository->findByLocation($player->getCurrentLocation());

        return $this->render('place/index.html.twig', [
            'places' => $places,
        ]);
    }
}
