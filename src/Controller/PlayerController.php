<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\EquipmentType;
use App\Entity\Item;
use App\Entity\Player;
use App\Entity\User;
use App\Form\PlayerType;
use App\Repository\PlayerItemRepository;
use App\Service\ItemService;
use App\Service\PlayerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class PlayerController extends AbstractController
{
    /**
     * @Route("/player", name="player_show")
     */
    public function show(): Response
    {
        return $this->render('player/show.html.twig', [
            'user' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/player/create", name="player_create")
     */
    public function create(Request $request, PlayerService $playerService): Response
    {
        $player = new Player();
        $form = $this->createForm(PlayerType::class, $player);
        $form->handleRequest($request);

        /** @var User $user */
        $user = $this->getUser();

        if ($form->isSubmitted() && $form->isValid()) {
            $playerService->createFullPlayer($player, $user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($player);
            $em->flush();

            return $this->redirectToRoute('app_homepage');
        }

        return $this->render('player/create.html.twig', [
            'user' => $user,
            'playerForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/player/equipment", name="player_equipment")
     */
    public function equipment(PlayerItemRepository $playerItemRepository): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        /** @var Player $player */
        $player = $user->getPlayer();

        return $this->render(
            'player/equipment.html.twig',
            [
                'player' => $player,
                'playerItemSlots' => $player->getPlayerItemSlots(),
                'playerItems' => $playerItemRepository->getPlayerItemsForPlayerByEqType(
                    $player,
                    EquipmentType::BAG_TYPE
                ),
            ]
        );
    }

    /**
     * @Route("/equipment/{id}/remove", name="equipment_remove")
     */
    public function removeItem(
        Item $item,
        ItemService $itemService
    ): RedirectResponse {
        /** @var User $user */
        $user = $this->getUser();
        /** @var Player $player */
        $player = $user->getPlayer();

        $itemService->removePlayerItem($player, $item);

        return $this->redirectToRoute('player_equipment');
    }
}
