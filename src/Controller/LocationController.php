<?php

namespace App\Controller;

use App\Entity\Location;
use App\Entity\Player;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class LocationController extends AbstractController
{
    /**
     * @Route("/location", name="location")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $locations = $em->getRepository(Location::class)->findAll();

        return $this->render('location/index.html.twig', [
            'locations' => $locations,
        ]);
    }

    /**
     * @Route("/location/{id}/travel", name="location_travel")
     * @param Location $location
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function travel(Location $location)
    {
        /** @var Player $player */
        $player = $this->getUser()->getPlayer();
        $player->setCurrentLocation($location);

        $em = $this->getDoctrine()->getManager();
        $em->persist($player);
        $em->flush();

        return $this->redirectToRoute('location_discover', [
            'id' => $location->getId(),
        ]);
    }

    /**
     * @Route("/location/{id}/discover", name="location_discover")
     * @param Location $location
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function discover(Location $location)
    {
        return $this->render('location/discover.html.twig', [
            'location' => $location,
        ]);
    }
}
