<?php

namespace App\Controller;

use App\Entity\EquipmentItem;
use App\Entity\Item;
use App\Entity\PlayerItem;
use App\Service\ItemService;
use App\Service\PlayerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ItemController extends AbstractController
{
    /**
     * @Route("/item/{id}/show", name="item_show")
     */
    public function show(Item $item)
    {
        return $this->render('item/show.html.twig', [
            'item' => $item,
        ]);
    }

    /**
     * @Route("/item/{id}/use", name="item_use")
     * @param PlayerItem $playerItem
     * @param ItemService $itemService
     * @param PlayerService $playerService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function useItem(
        PlayerItem $playerItem,
        ItemService $itemService,
        PlayerService $playerService
    )
    {
        $itemService->useItemOnPlayer($this->getUser()->getPlayer(), $playerItem);

        return $this->redirectToRoute('player_equipment');
    }
}
