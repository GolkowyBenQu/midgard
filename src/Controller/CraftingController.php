<?php

namespace App\Controller;

use App\Entity\EquipmentItem;
use App\Entity\Receipe;
use App\Entity\User;
use App\Service\CraftingService;
use App\Service\ItemService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CraftingController extends AbstractController
{
    /**
     * @Route("/crafting", name="crafting")
     * @param CraftingService $craftingService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(CraftingService $craftingService)
    {
        $player = $this->getUser()->getPlayer();

        return $this->render(
            'crafting/index.html.twig',
            [
                'receipes' => $craftingService->getReceipesForPlayer($player),
            ]
        );
    }

    /**
     * @Route("/crafting/{id}/create", name="crafting_create")
     * @param Receipe $receipe
     * @param CraftingService $craftingService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function create(
        Receipe $receipe,
        CraftingService $craftingService
    ) {
        /** @var User $user */
        $user = $this->getUser();

        $craftingService->craftRecipe($user->getPlayer(), $receipe);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $this->addFlash('success', 'Item crafted');

        return $this->redirectToRoute('crafting');
    }
}
