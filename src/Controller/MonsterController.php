<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Item;
use App\Entity\Monster;
use App\Entity\Player;
use App\Entity\User;
use App\Service\FightService;
use App\Service\ItemService;
use App\Service\LootService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class MonsterController extends AbstractController
{
    /**
     * @Route("/monster/{id}/inspect", name="monster_inspect")
     */
    public function inspect(Monster $monster): Response
    {
        return $this->render('monster/inspect.html.twig', [
            'monster' => $monster,
        ]);
    }

    /**
     * @Route("/monster/{id}/fight", name="monster_fight")
     * @param Monster $monster
     * @param LootService $lootService
     * @param ItemService $itemService
     * @param FightService $fightService
     * @return Response
     */
    public function fight(
        Monster $monster,
        LootService $lootService,
        ItemService $itemService,
        FightService $fightService
    ): Response {
        /** @var User $user */
        $user = $this->getUser();
        /** @var Player $player */
        $player = $user->getPlayer();

        $fightService->fightWithMonster($player, $monster);
        $lootedItems = $lootService->randomLoot($monster);

        foreach ($lootedItems as $ltItem) {
            $itemService->givePlayerItem($player, $ltItem['item'], $ltItem['count']);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($player);
        $em->flush();

        return $this->render('monster/fight.html.twig', [
            'player' => $player,
            'monster' => $monster,
            'report' => $fightService->actions,
            'lootItems' => $lootedItems,
        ]);
    }
}
