<?php

namespace App\DataFixtures;

use App\Entity\Player;
use App\Entity\PlayerAttribute;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PlayerAttributeFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $playerAttribute = new PlayerAttribute();
        $playerAttribute->setPlayer($this->getReference('user-player'));
        $playerAttribute->setAttribute($this->getReference('life-attribute'));
        $playerAttribute->setBaseValue(Player::DEFAULT_LIFE);
        $playerAttribute->setCurrentValue(Player::DEFAULT_LIFE);
        $manager->persist($playerAttribute);

        $playerAttribute = new PlayerAttribute();
        $playerAttribute->setPlayer($this->getReference('user-player'));
        $playerAttribute->setAttribute($this->getReference('damage-attribute'));
        $playerAttribute->setBaseValue(Player::DEFAULT_DAMAGE);
        $playerAttribute->setCurrentValue(Player::DEFAULT_DAMAGE);
        $manager->persist($playerAttribute);

        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            PlayerFixtures::class,
            AttributeFixtures::class,
        ];
    }
}
