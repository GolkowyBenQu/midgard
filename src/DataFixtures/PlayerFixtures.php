<?php

namespace App\DataFixtures;

use App\Entity\Player;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PlayerFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $player = new Player();
        $player->setName('user player');
        $player->setUser($this->getReference('default-user'));
        $manager->persist($player);
        $this->addReference('user-player', $player);

        $player = new Player();
        $player->setName('admin player');
        $player->setUser($this->getReference('admin-user'));
        $manager->persist($player);
        $this->addReference('admin-player', $player);

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
