<?php

namespace App\DataFixtures;

use App\Entity\Attribute;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AttributeFixtures extends Fixture
{
    private $virtualAttributes = [
        Attribute::DAMAGE_ATTRIBUTE => 1,
        Attribute::DEFENSE_ATTRIBUTE => 1,
        Attribute::SPEED_ATTRIBUTE => 1,
        Attribute::LIFE_ATTRIBUTE => 100,
        Attribute::STRENGTH_ATTRIBUTE => 5,
        Attribute::DEXTERITY_ATTRIBUTE => 5,
        Attribute::VITALITY_ATTRIBUTE => 20,
    ];
    private $basicAttributes = [
        Attribute::MANA_ATTRIBUTE => 50,
        Attribute::ENERGY_ATTRIBUTE => 15,
    ];

    public function load(ObjectManager $manager)
    {
        foreach ($this->virtualAttributes as $attributeName => $pointValue) {
            $attr = new Attribute();
            $attr->setName($attributeName);
            $attr->setPointValue($pointValue);
            $manager->persist($attr);
            $this->addReference($attributeName.'-attribute', $attr);
        }
        foreach ($this->basicAttributes as $attributeName => $pointValue) {
            $attr = new Attribute();
            $attr->setName($attributeName);
            $attr->setPointValue($pointValue);
            $attr->setIsBasic(true);
            $manager->persist($attr);
            $this->addReference($attributeName.'-attribute', $attr);
        }

        $manager->flush();
    }
}
