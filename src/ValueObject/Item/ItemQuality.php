<?php

declare(strict_types=1);

namespace App\ValueObject\Item;

final class ItemQuality
{
    public const POOR = 'poor';
    public const COMMON = 'common';
}
