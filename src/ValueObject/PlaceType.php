<?php

declare(strict_types=1);

namespace App\ValueObject;

final class PlaceType
{
    public const SHOP = 'shop';
    public const DUNGEON = 'dungeon';
}
