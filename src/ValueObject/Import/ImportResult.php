<?php

declare(strict_types=1);

namespace App\ValueObject\Import;

final class ImportResult
{
    public const ERROR_MESSAGES = 'error_messages';
    public const INFO_MESSAGES = 'info_messages';

    public const ADDED_ROWS_COUNT = 'added_rows_count';
    public const INVALID_ROWS_COUNT = 'invalid_rows_count';
    public const UPDATED_ROWS_COUNT = 'updated_rows_count';
    public const REMOVED_ROWS_COUNT = 'removed_rows_count';

    public const CATEGORIES_COUNT = 'categories_count';
    public const ATTRIBUTES_COUNT = 'attributes_count';
    public const ATTRIBUTE_VALUES_COUNT = 'attribute_values_count';

    public const RESULT_FILENAME = 'result_filename';
}
