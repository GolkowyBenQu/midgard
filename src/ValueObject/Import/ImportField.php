<?php

declare(strict_types=1);

namespace App\ValueObject\Import;

final class ImportField
{
    public const IMPORT_ID_FIELD = 'id';
    public const NAME_FIELD = 'name';
    public const PRICE_FIELD = 'price';
    public const WEARABLE_FIELD = 'wearable';
    public const USABLE_FIELD = 'usable';
    public const TYPE_FIELD = 'type';
    public const QUALITY_FIELD = 'quality';
    public const SLOT_FIELD = 'slot';
    public const STACK_SIZE_FIELD = 'stack-size';
}
