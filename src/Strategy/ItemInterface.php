<?php

namespace App\Strategy;

use App\Entity\Player;
use App\Entity\PlayerItem;
use Doctrine\ORM\EntityManagerInterface;

interface ItemInterface
{
    public function __construct(EntityManagerInterface $entityManager);

    public function updatePlayer(Player $player, PlayerItem $playerItem): void;
}
