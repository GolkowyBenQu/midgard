<?php

namespace App\Strategy\Item;

use App\Entity\EquipmentType;
use App\Entity\Player;
use App\Entity\PlayerItem;
use App\Entity\PlayerItemSlot;
use App\Strategy\ItemInterface;
use Doctrine\ORM\EntityManagerInterface;

class ItemSlotStrategy implements ItemInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function updatePlayer(Player $player, PlayerItem $playerItem): void
    {
        $itemSlot = $playerItem->getItem()->getSlot();
        $playerItemSlot = $player->getPlayerItemSlot($itemSlot->getName());

        if (!$playerItemSlot) {
            $playerItemSlot = new PlayerItemSlot();
            $playerItemSlot->setSlot($playerItem->getItem()->getSlot());
            $player->addPlayerItemSlot($playerItemSlot);
        }

        if ($playerItemSlot->getPlayerItem()) {
            $this->takeOff($player, $playerItemSlot->getPlayerItem());
        }

        $this->takeOn($player, $playerItem);

        $this->entityManager->persist($player);
        $this->entityManager->persist($playerItem);
    }

    private function takeOn(Player $player, PlayerItem $playerItem)
    {
        $eqSlotType = $this->entityManager->getRepository(EquipmentType::class)->findOneByName(EquipmentType::SLOT_TYPE);

        $itemSlot = $playerItem->getItem()->getSlot();
        $playerItemSlot = $player->getPlayerItemSlot($itemSlot->getName());
        $playerItemSlot->setPlayerItem($playerItem);
        $playerItem->setEquipmentType($eqSlotType);
    }

    private function takeOff(Player $player, PlayerItem $playerItem)
    {
        $eqBagType = $this->entityManager->getRepository(EquipmentType::class)->findOneByName(EquipmentType::BAG_TYPE);

        $itemSlot = $playerItem->getItem()->getSlot();
        $playerItemSlot = $player->getPlayerItemSlot($itemSlot->getName());
        $playerItemSlot->setPlayerItem(null);
        $playerItem->setEquipmentType($eqBagType);
    }
}
