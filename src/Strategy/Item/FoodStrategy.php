<?php

namespace App\Strategy\Item;

use App\Entity\Player;
use App\Entity\PlayerItem;
use App\Strategy\ItemInterface;
use Doctrine\ORM\EntityManagerInterface;

class FoodStrategy implements ItemInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function updatePlayer(Player $player, PlayerItem $playerItem): void
    {
        $newItemCount = $playerItem->getCount() - 1;

        if ($playerItem->getItem()->getStackSize() > 0 && $newItemCount <= 0) {
            $this->entityManager->remove($playerItem);
        } else {
            $playerItem->setCount($newItemCount);
            $this->entityManager->persist($playerItem);
        }

        $itemAttributes = $playerItem->getItem()->getItemAttributes();
        foreach ($itemAttributes as $itemAttribute) {
            $playerAttribute = $player->getPlayerAttribute($itemAttribute->getAttribute()->getName());

            $newValue = $playerAttribute->getCurrentValue() + $itemAttribute->getValue();
            if ($newValue > $playerAttribute->getBaseValue()) {
                $newValue = $playerAttribute->getBaseValue();
            }

            $playerAttribute->setCurrentValue($newValue);
            $this->entityManager->persist($playerAttribute);
        }
    }
}
