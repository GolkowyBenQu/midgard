<?php

namespace App\Strategy;

use App\Entity\Item;
use App\Entity\ItemType;
use App\Strategy\Item\FoodStrategy;
use App\Strategy\Item\ItemSlotStrategy;
use Doctrine\ORM\EntityManagerInterface;

class ItemManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Item $item
     * @return ItemInterface
     */
    public function getStrategy(Item $item): ItemInterface
    {
        if ($item->getType()->getName() === ItemType::WEAPON_TYPE) {
            return new ItemSlotStrategy($this->entityManager);
        }

        if ($item->getType()->getName() === ItemType::ARMOR_TYPE) {
            return new ItemSlotStrategy($this->entityManager);
        }

        if ($item->getType()->getName() === ItemType::FOOD_TYPE) {
            return new FoodStrategy($this->entityManager);
        }

        return new ItemSlotStrategy($this->entityManager);
    }
}
