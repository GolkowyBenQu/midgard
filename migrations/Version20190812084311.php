<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190812084311 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE equipment_item_slot (id INT AUTO_INCREMENT NOT NULL, item_id INT NOT NULL, slot_id INT NOT NULL, player_id INT NOT NULL, INDEX IDX_B60322E5126F525E (item_id), INDEX IDX_B60322E559E5119C (slot_id), INDEX IDX_B60322E599E6F5DF (player_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE equipment_item_slot ADD CONSTRAINT FK_B60322E5126F525E FOREIGN KEY (item_id) REFERENCES equipment_item (id)');
        $this->addSql('ALTER TABLE equipment_item_slot ADD CONSTRAINT FK_B60322E559E5119C FOREIGN KEY (slot_id) REFERENCES item_slot (id)');
        $this->addSql('ALTER TABLE equipment_item_slot ADD CONSTRAINT FK_B60322E599E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE item ADD slot_id INT NOT NULL');
        $this->addSql('ALTER TABLE item ADD CONSTRAINT FK_1F1B251E59E5119C FOREIGN KEY (slot_id) REFERENCES item_slot (id)');
        $this->addSql('CREATE INDEX IDX_1F1B251E59E5119C ON item (slot_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE equipment_item_slot');
        $this->addSql('ALTER TABLE item DROP FOREIGN KEY FK_1F1B251E59E5119C');
        $this->addSql('DROP INDEX IDX_1F1B251E59E5119C ON item');
        $this->addSql('ALTER TABLE item DROP slot_id');
    }
}
