<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211002182148 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE player_attribute DROP FOREIGN KEY FK_5A76DAC299E6F5DF');
        $this->addSql('ALTER TABLE player_attribute ADD CONSTRAINT FK_5A76DAC299E6F5DF FOREIGN KEY (player_id) REFERENCES player (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE player_item DROP FOREIGN KEY FK_BD01752D99E6F5DF');
        $this->addSql('ALTER TABLE player_item ADD CONSTRAINT FK_BD01752D99E6F5DF FOREIGN KEY (player_id) REFERENCES player (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE player_item_slot DROP FOREIGN KEY FK_C43251A599E6F5DF');
        $this->addSql('ALTER TABLE player_item_slot ADD CONSTRAINT FK_C43251A599E6F5DF FOREIGN KEY (player_id) REFERENCES player (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE player_attribute DROP FOREIGN KEY FK_5A76DAC299E6F5DF');
        $this->addSql('ALTER TABLE player_attribute ADD CONSTRAINT FK_5A76DAC299E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE player_item DROP FOREIGN KEY FK_BD01752D99E6F5DF');
        $this->addSql('ALTER TABLE player_item ADD CONSTRAINT FK_BD01752D99E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE player_item_slot DROP FOREIGN KEY FK_C43251A599E6F5DF');
        $this->addSql('ALTER TABLE player_item_slot ADD CONSTRAINT FK_C43251A599E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
