<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190331211004 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE place (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE place_object (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE place_object_place (place_object_id INT NOT NULL, place_id INT NOT NULL, INDEX IDX_218B959657B6DF56 (place_object_id), INDEX IDX_218B9596DA6A219 (place_id), PRIMARY KEY(place_object_id, place_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE place_object_place ADD CONSTRAINT FK_218B959657B6DF56 FOREIGN KEY (place_object_id) REFERENCES place_object (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE place_object_place ADD CONSTRAINT FK_218B9596DA6A219 FOREIGN KEY (place_id) REFERENCES place (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE player ADD current_place_id INT NOT NULL');
        $this->addSql('ALTER TABLE player ADD CONSTRAINT FK_98197A6590DED833 FOREIGN KEY (current_place_id) REFERENCES place (id)');
        $this->addSql('CREATE INDEX IDX_98197A6590DED833 ON player (current_place_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE place_object_place DROP FOREIGN KEY FK_218B9596DA6A219');
        $this->addSql('ALTER TABLE player DROP FOREIGN KEY FK_98197A6590DED833');
        $this->addSql('ALTER TABLE place_object_place DROP FOREIGN KEY FK_218B959657B6DF56');
        $this->addSql('DROP TABLE place');
        $this->addSql('DROP TABLE place_object');
        $this->addSql('DROP TABLE place_object_place');
        $this->addSql('DROP INDEX IDX_98197A6590DED833 ON player');
        $this->addSql('ALTER TABLE player DROP current_place_id');
    }
}
