<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211003171434 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE item DROP FOREIGN KEY FK_1F1B251E59E5119C');
        $this->addSql('ALTER TABLE item DROP FOREIGN KEY FK_1F1B251EBCFC6D57');
        $this->addSql('ALTER TABLE item DROP FOREIGN KEY FK_1F1B251EC54C8C93');
        $this->addSql('DROP INDEX IDX_1F1B251EBCFC6D57 ON item');
        $this->addSql('DROP INDEX IDX_1F1B251E59E5119C ON item');
        $this->addSql('DROP INDEX IDX_1F1B251EC54C8C93 ON item');
        $this->addSql('ALTER TABLE item ADD usable TINYINT(1) NOT NULL, ADD type VARCHAR(255) NOT NULL, ADD quality VARCHAR(255) NOT NULL, ADD slot VARCHAR(255) NOT NULL, ADD import_id INT NOT NULL, DROP type_id, DROP quality_id, DROP slot_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE item ADD quality_id INT NOT NULL, ADD slot_id INT DEFAULT NULL, DROP usable, DROP type, DROP quality, DROP slot, CHANGE import_id type_id INT NOT NULL');
        $this->addSql('ALTER TABLE item ADD CONSTRAINT FK_1F1B251E59E5119C FOREIGN KEY (slot_id) REFERENCES item_slot (id)');
        $this->addSql('ALTER TABLE item ADD CONSTRAINT FK_1F1B251EBCFC6D57 FOREIGN KEY (quality_id) REFERENCES item_quality (id)');
        $this->addSql('ALTER TABLE item ADD CONSTRAINT FK_1F1B251EC54C8C93 FOREIGN KEY (type_id) REFERENCES item_type (id)');
        $this->addSql('CREATE INDEX IDX_1F1B251EBCFC6D57 ON item (quality_id)');
        $this->addSql('CREATE INDEX IDX_1F1B251E59E5119C ON item (slot_id)');
        $this->addSql('CREATE INDEX IDX_1F1B251EC54C8C93 ON item (type_id)');
    }
}
