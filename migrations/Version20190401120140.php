<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190401120140 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE place_object_place DROP FOREIGN KEY FK_218B959657B6DF56');
        $this->addSql('CREATE TABLE `drop` (id INT AUTO_INCREMENT NOT NULL, monster_id INT NOT NULL, item_id INT NOT NULL, chance SMALLINT NOT NULL, INDEX IDX_70150522C5FF1223 (monster_id), INDEX IDX_70150522126F525E (item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE location (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE location_place (location_id INT NOT NULL, place_id INT NOT NULL, INDEX IDX_24116E864D218E (location_id), INDEX IDX_24116E8DA6A219 (place_id), PRIMARY KEY(location_id, place_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE location_monster (location_id INT NOT NULL, monster_id INT NOT NULL, INDEX IDX_CA1BD55064D218E (location_id), INDEX IDX_CA1BD550C5FF1223 (monster_id), PRIMARY KEY(location_id, monster_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `drop` ADD CONSTRAINT FK_70150522C5FF1223 FOREIGN KEY (monster_id) REFERENCES monster (id)');
        $this->addSql('ALTER TABLE `drop` ADD CONSTRAINT FK_70150522126F525E FOREIGN KEY (item_id) REFERENCES item (id)');
        $this->addSql('ALTER TABLE location_place ADD CONSTRAINT FK_24116E864D218E FOREIGN KEY (location_id) REFERENCES location (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE location_place ADD CONSTRAINT FK_24116E8DA6A219 FOREIGN KEY (place_id) REFERENCES place (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE location_monster ADD CONSTRAINT FK_CA1BD55064D218E FOREIGN KEY (location_id) REFERENCES location (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE location_monster ADD CONSTRAINT FK_CA1BD550C5FF1223 FOREIGN KEY (monster_id) REFERENCES monster (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE monster_place');
        $this->addSql('DROP TABLE place_object');
        $this->addSql('DROP TABLE place_object_place');
        $this->addSql('ALTER TABLE level CHANGE point_limit `limit` INT NOT NULL');
        $this->addSql('ALTER TABLE player DROP FOREIGN KEY FK_98197A6590DED833');
        $this->addSql('DROP INDEX IDX_98197A6590DED833 ON player');
        $this->addSql('ALTER TABLE player ADD current_location_id INT DEFAULT NULL, ADD experience BIGINT DEFAULT NULL, DROP current_place_id, DROP points');
        $this->addSql('ALTER TABLE player ADD CONSTRAINT FK_98197A65B8998A57 FOREIGN KEY (current_location_id) REFERENCES location (id)');
        $this->addSql('CREATE INDEX IDX_98197A65B8998A57 ON player (current_location_id)');
        $this->addSql('ALTER TABLE item DROP FOREIGN KEY FK_1F1B251EBF6ADCA3');
        $this->addSql('DROP INDEX IDX_1F1B251EBF6ADCA3 ON item');
        $this->addSql('ALTER TABLE item DROP dropping_monster_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE location_place DROP FOREIGN KEY FK_24116E864D218E');
        $this->addSql('ALTER TABLE location_monster DROP FOREIGN KEY FK_CA1BD55064D218E');
        $this->addSql('ALTER TABLE player DROP FOREIGN KEY FK_98197A65B8998A57');
        $this->addSql('CREATE TABLE monster_place (monster_id INT NOT NULL, place_id INT NOT NULL, INDEX IDX_2E56F88C5FF1223 (monster_id), INDEX IDX_2E56F88DA6A219 (place_id), PRIMARY KEY(monster_id, place_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE place_object (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE place_object_place (place_object_id INT NOT NULL, place_id INT NOT NULL, INDEX IDX_218B959657B6DF56 (place_object_id), INDEX IDX_218B9596DA6A219 (place_id), PRIMARY KEY(place_object_id, place_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE monster_place ADD CONSTRAINT FK_2E56F88C5FF1223 FOREIGN KEY (monster_id) REFERENCES monster (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE monster_place ADD CONSTRAINT FK_2E56F88DA6A219 FOREIGN KEY (place_id) REFERENCES place (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE place_object_place ADD CONSTRAINT FK_218B959657B6DF56 FOREIGN KEY (place_object_id) REFERENCES place_object (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE place_object_place ADD CONSTRAINT FK_218B9596DA6A219 FOREIGN KEY (place_id) REFERENCES place (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE `drop`');
        $this->addSql('DROP TABLE location');
        $this->addSql('DROP TABLE location_place');
        $this->addSql('DROP TABLE location_monster');
        $this->addSql('ALTER TABLE item ADD dropping_monster_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE item ADD CONSTRAINT FK_1F1B251EBF6ADCA3 FOREIGN KEY (dropping_monster_id) REFERENCES monster (id)');
        $this->addSql('CREATE INDEX IDX_1F1B251EBF6ADCA3 ON item (dropping_monster_id)');
        $this->addSql('ALTER TABLE level CHANGE `limit` point_limit INT NOT NULL');
        $this->addSql('DROP INDEX IDX_98197A65B8998A57 ON player');
        $this->addSql('ALTER TABLE player ADD current_place_id INT NOT NULL, ADD points DOUBLE PRECISION DEFAULT NULL, DROP current_location_id, DROP experience');
        $this->addSql('ALTER TABLE player ADD CONSTRAINT FK_98197A6590DED833 FOREIGN KEY (current_place_id) REFERENCES place (id)');
        $this->addSql('CREATE INDEX IDX_98197A6590DED833 ON player (current_place_id)');
    }
}
