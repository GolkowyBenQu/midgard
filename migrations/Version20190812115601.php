<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190812115601 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE equipment_item_slot DROP FOREIGN KEY FK_B60322E5126F525E');
        $this->addSql('DROP INDEX IDX_B60322E5126F525E ON equipment_item_slot');
        $this->addSql('ALTER TABLE equipment_item_slot CHANGE item_id equipment_item_id INT NOT NULL');
        $this->addSql('ALTER TABLE equipment_item_slot ADD CONSTRAINT FK_B60322E564149DB1 FOREIGN KEY (equipment_item_id) REFERENCES equipment_item (id)');
        $this->addSql('CREATE INDEX IDX_B60322E564149DB1 ON equipment_item_slot (equipment_item_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE equipment_item_slot DROP FOREIGN KEY FK_B60322E564149DB1');
        $this->addSql('DROP INDEX IDX_B60322E564149DB1 ON equipment_item_slot');
        $this->addSql('ALTER TABLE equipment_item_slot CHANGE equipment_item_id item_id INT NOT NULL');
        $this->addSql('ALTER TABLE equipment_item_slot ADD CONSTRAINT FK_B60322E5126F525E FOREIGN KEY (item_id) REFERENCES equipment_item (id)');
        $this->addSql('CREATE INDEX IDX_B60322E5126F525E ON equipment_item_slot (item_id)');
    }
}
