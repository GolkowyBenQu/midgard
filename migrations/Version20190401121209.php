<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190401121209 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE loot (id INT AUTO_INCREMENT NOT NULL, monster_id INT NOT NULL, item_id INT NOT NULL, chance SMALLINT NOT NULL, INDEX IDX_A632D9F7C5FF1223 (monster_id), INDEX IDX_A632D9F7126F525E (item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE loot ADD CONSTRAINT FK_A632D9F7C5FF1223 FOREIGN KEY (monster_id) REFERENCES monster (id)');
        $this->addSql('ALTER TABLE loot ADD CONSTRAINT FK_A632D9F7126F525E FOREIGN KEY (item_id) REFERENCES item (id)');
        $this->addSql('DROP TABLE `drop`');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `drop` (id INT AUTO_INCREMENT NOT NULL, monster_id INT NOT NULL, item_id INT NOT NULL, chance SMALLINT NOT NULL, INDEX IDX_70150522C5FF1223 (monster_id), INDEX IDX_70150522126F525E (item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE `drop` ADD CONSTRAINT FK_70150522126F525E FOREIGN KEY (item_id) REFERENCES item (id)');
        $this->addSql('ALTER TABLE `drop` ADD CONSTRAINT FK_70150522C5FF1223 FOREIGN KEY (monster_id) REFERENCES monster (id)');
        $this->addSql('DROP TABLE loot');
    }
}
