<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190402164946 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE item ADD wearable TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE monster ADD life INT NOT NULL, ADD damage INT NOT NULL, ADD min_level INT NOT NULL, ADD max_level INT NOT NULL');
        $this->addSql('ALTER TABLE player ADD base_life INT NOT NULL, ADD current_life INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE item DROP wearable');
        $this->addSql('ALTER TABLE monster DROP life, DROP damage, DROP min_level, DROP max_level');
        $this->addSql('ALTER TABLE player DROP base_life, DROP current_life');
    }
}
