<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211003173028 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ingredient DROP FOREIGN KEY FK_6BAF7870126F525E');
        $this->addSql('ALTER TABLE ingredient DROP FOREIGN KEY FK_6BAF7870C3F9986C');
        $this->addSql('ALTER TABLE ingredient ADD CONSTRAINT FK_6BAF7870126F525E FOREIGN KEY (item_id) REFERENCES item (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ingredient ADD CONSTRAINT FK_6BAF7870C3F9986C FOREIGN KEY (receipe_id) REFERENCES receipe (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE item_attribute DROP FOREIGN KEY FK_F6A0F90BB6E62EFA');
        $this->addSql('ALTER TABLE item_attribute ADD CONSTRAINT FK_F6A0F90BB6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE loot DROP FOREIGN KEY FK_A632D9F7126F525E');
        $this->addSql('ALTER TABLE loot DROP FOREIGN KEY FK_A632D9F7C5FF1223');
        $this->addSql('ALTER TABLE loot ADD CONSTRAINT FK_A632D9F7126F525E FOREIGN KEY (item_id) REFERENCES item (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE loot ADD CONSTRAINT FK_A632D9F7C5FF1223 FOREIGN KEY (monster_id) REFERENCES monster (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE race_attribute DROP FOREIGN KEY FK_23FCAFE66E59D40D');
        $this->addSql('ALTER TABLE race_attribute ADD CONSTRAINT FK_23FCAFE66E59D40D FOREIGN KEY (race_id) REFERENCES race (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE receipe DROP FOREIGN KEY FK_392996B7126F525E');
        $this->addSql('ALTER TABLE receipe ADD CONSTRAINT FK_392996B7126F525E FOREIGN KEY (item_id) REFERENCES item (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ingredient DROP FOREIGN KEY FK_6BAF7870C3F9986C');
        $this->addSql('ALTER TABLE ingredient DROP FOREIGN KEY FK_6BAF7870126F525E');
        $this->addSql('ALTER TABLE ingredient ADD CONSTRAINT FK_6BAF7870C3F9986C FOREIGN KEY (receipe_id) REFERENCES receipe (id)');
        $this->addSql('ALTER TABLE ingredient ADD CONSTRAINT FK_6BAF7870126F525E FOREIGN KEY (item_id) REFERENCES item (id)');
        $this->addSql('ALTER TABLE item_attribute DROP FOREIGN KEY FK_F6A0F90BB6E62EFA');
        $this->addSql('ALTER TABLE item_attribute ADD CONSTRAINT FK_F6A0F90BB6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id)');
        $this->addSql('ALTER TABLE loot DROP FOREIGN KEY FK_A632D9F7C5FF1223');
        $this->addSql('ALTER TABLE loot DROP FOREIGN KEY FK_A632D9F7126F525E');
        $this->addSql('ALTER TABLE loot ADD CONSTRAINT FK_A632D9F7C5FF1223 FOREIGN KEY (monster_id) REFERENCES monster (id)');
        $this->addSql('ALTER TABLE loot ADD CONSTRAINT FK_A632D9F7126F525E FOREIGN KEY (item_id) REFERENCES item (id)');
        $this->addSql('ALTER TABLE race_attribute DROP FOREIGN KEY FK_23FCAFE66E59D40D');
        $this->addSql('ALTER TABLE race_attribute ADD CONSTRAINT FK_23FCAFE66E59D40D FOREIGN KEY (race_id) REFERENCES race (id)');
        $this->addSql('ALTER TABLE receipe DROP FOREIGN KEY FK_392996B7126F525E');
        $this->addSql('ALTER TABLE receipe ADD CONSTRAINT FK_392996B7126F525E FOREIGN KEY (item_id) REFERENCES item (id)');
    }
}
