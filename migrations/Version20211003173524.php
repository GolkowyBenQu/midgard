<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211003173524 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE player_item DROP FOREIGN KEY FK_BD01752D126F525E');
        $this->addSql('ALTER TABLE player_item ADD CONSTRAINT FK_BD01752D126F525E FOREIGN KEY (item_id) REFERENCES item (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE player_item DROP FOREIGN KEY FK_BD01752D126F525E');
        $this->addSql('ALTER TABLE player_item ADD CONSTRAINT FK_BD01752D126F525E FOREIGN KEY (item_id) REFERENCES item (id)');
    }
}
