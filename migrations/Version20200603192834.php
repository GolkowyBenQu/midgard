<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200603192834 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE equipment_item DROP FOREIGN KEY FK_259FF418517FE9FE');
        $this->addSql('ALTER TABLE equipment_item_slot DROP FOREIGN KEY FK_B60322E564149DB1');
        $this->addSql('CREATE TABLE player_item (id INT AUTO_INCREMENT NOT NULL, player_id INT NOT NULL, item_id INT NOT NULL, equipment_type_id INT NOT NULL, count INT DEFAULT NULL, INDEX IDX_BD01752D99E6F5DF (player_id), INDEX IDX_BD01752D126F525E (item_id), INDEX IDX_BD01752DB337437C (equipment_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE player_item_slot (id INT AUTO_INCREMENT NOT NULL, player_item_id INT NOT NULL, slot_id INT NOT NULL, player_id INT NOT NULL, INDEX IDX_C43251A5739BAD1B (player_item_id), INDEX IDX_C43251A559E5119C (slot_id), INDEX IDX_C43251A599E6F5DF (player_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE player_item ADD CONSTRAINT FK_BD01752D99E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE player_item ADD CONSTRAINT FK_BD01752D126F525E FOREIGN KEY (item_id) REFERENCES item (id)');
        $this->addSql('ALTER TABLE player_item ADD CONSTRAINT FK_BD01752DB337437C FOREIGN KEY (equipment_type_id) REFERENCES equipment_type (id)');
        $this->addSql('ALTER TABLE player_item_slot ADD CONSTRAINT FK_C43251A5739BAD1B FOREIGN KEY (player_item_id) REFERENCES player_item (id)');
        $this->addSql('ALTER TABLE player_item_slot ADD CONSTRAINT FK_C43251A559E5119C FOREIGN KEY (slot_id) REFERENCES item_slot (id)');
        $this->addSql('ALTER TABLE player_item_slot ADD CONSTRAINT FK_C43251A599E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('DROP TABLE equipment');
        $this->addSql('DROP TABLE equipment_item');
        $this->addSql('DROP TABLE equipment_item_slot');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE player_item_slot DROP FOREIGN KEY FK_C43251A5739BAD1B');
        $this->addSql('CREATE TABLE equipment (id INT AUTO_INCREMENT NOT NULL, owner_id INT NOT NULL, type_id INT NOT NULL, INDEX IDX_D338D583C54C8C93 (type_id), UNIQUE INDEX UNIQ_D338D5837E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE equipment_item (id INT AUTO_INCREMENT NOT NULL, equipment_id INT NOT NULL, item_id INT NOT NULL, count INT NOT NULL, INDEX IDX_259FF418126F525E (item_id), INDEX IDX_259FF418517FE9FE (equipment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE equipment_item_slot (id INT AUTO_INCREMENT NOT NULL, equipment_item_id INT NOT NULL, slot_id INT NOT NULL, player_id INT NOT NULL, INDEX IDX_B60322E559E5119C (slot_id), INDEX IDX_B60322E564149DB1 (equipment_item_id), INDEX IDX_B60322E599E6F5DF (player_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE equipment ADD CONSTRAINT FK_D338D5837E3C61F9 FOREIGN KEY (owner_id) REFERENCES player (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE equipment ADD CONSTRAINT FK_D338D583C54C8C93 FOREIGN KEY (type_id) REFERENCES equipment_type (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE equipment_item ADD CONSTRAINT FK_259FF418126F525E FOREIGN KEY (item_id) REFERENCES item (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE equipment_item ADD CONSTRAINT FK_259FF418517FE9FE FOREIGN KEY (equipment_id) REFERENCES equipment (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE equipment_item_slot ADD CONSTRAINT FK_B60322E559E5119C FOREIGN KEY (slot_id) REFERENCES item_slot (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE equipment_item_slot ADD CONSTRAINT FK_B60322E564149DB1 FOREIGN KEY (equipment_item_id) REFERENCES equipment_item (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE equipment_item_slot ADD CONSTRAINT FK_B60322E599E6F5DF FOREIGN KEY (player_id) REFERENCES player (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('DROP TABLE player_item');
        $this->addSql('DROP TABLE player_item_slot');
    }
}
